package com.cmbk.email.handlers;

public class EmployeeServiceException extends RuntimeException {

    private static final long serialVersionUID = 5776681206288518465L;

    public EmployeeServiceException(String message) {
        super(message);
    }

}
