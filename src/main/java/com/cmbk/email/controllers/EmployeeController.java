package com.cmbk.email.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cmbk.email.controllers.common.HTTPResponseHandler;
import com.cmbk.email.controllers.common.RequestMappings;
import com.cmbk.email.domain.EmployeeValidator;
import com.cmbk.email.domain.SendEmployeeSalaryDetailsRequest;
import com.cmbk.email.domain.SendEmployeeSalaryDetailsResponse;
import com.cmbk.email.services.EmployeeService;

/**
 * @author chanaka.k
 *
 */
@RestController
@RequestMapping(RequestMappings.EMPLOYEES)
public class EmployeeController extends HTTPResponseHandler {

	private final String EMPLOYEE_ENDPOINTS_RUNNING = "Employee Email Service endpoints are running";

	final static Logger logger = Logger.getLogger(EmployeeController.class);

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value = RequestMappings.CONTEXT_PATH, method = RequestMethod.GET)
	public String plainRequest() {
		return EMPLOYEE_ENDPOINTS_RUNNING;
	}

	/**
	 * Send Email
	 *
	 */
	@RequestMapping(value = RequestMappings.SEND_EMPLOYEE_SALARY_EMAIL, method = RequestMethod.POST)
	public @ResponseBody SendEmployeeSalaryDetailsResponse sendEmpSalaryEmail(
			@RequestBody SendEmployeeSalaryDetailsRequest request) {

		EmployeeValidator.validateSendEmployeeSalaryDetailsRequest(request);

		return employeeService.sendEmployeeSalaryEmail(request);

	}

}
