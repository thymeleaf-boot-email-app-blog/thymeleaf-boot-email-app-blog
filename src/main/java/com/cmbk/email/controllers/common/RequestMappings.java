package com.cmbk.email.controllers.common;

public interface RequestMappings {

	public static String CONTEXT_PATH = "/";

	public static String EMPLOYEES = "employees";

	public static String SEND_EMPLOYEE_SALARY_EMAIL = "/salary/email";

}
