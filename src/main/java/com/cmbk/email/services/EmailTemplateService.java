package com.cmbk.email.services;

/**
 * @author chanaka.k
 *
 */
public interface EmailTemplateService {

	/**
	 * Gets employee salary detail mail body template.
	 *
	 * @param salaryAmount the salary amount
	 * @return string the email update success body template
	 */
	String getEmployeeSalaryDetailBodyTemplate(Double salaryAmount);

}
