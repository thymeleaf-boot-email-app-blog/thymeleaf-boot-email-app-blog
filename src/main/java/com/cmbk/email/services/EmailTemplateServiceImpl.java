package com.cmbk.email.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * @author chanaka.k
 *
 */
@Service
public class EmailTemplateServiceImpl implements EmailTemplateService {

	@Autowired
	private TemplateEngine templateEngine;

	@Override
	public String getEmployeeSalaryDetailBodyTemplate(Double salaryAmount) {

		Context ctx = new Context();
		ctx.setVariable("salaryAmount", salaryAmount);
		ctx.setVariable("date", new Date());

		return templateEngine.process("salaryDetailEmailTemplate", ctx);

	}

}
