package com.cmbk.email.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cmbk.email.domain.SendEmailRequest;
import com.cmbk.email.domain.SendEmailResponse;
import com.cmbk.email.domain.SendEmployeeSalaryDetailsRequest;
import com.cmbk.email.domain.SendEmployeeSalaryDetailsResponse;
import com.cmbk.email.domain.Status;

/**
 * @author chanaka.k
 *
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmailTemplateService mailTemplateService;

	@Autowired
	private EmailService emailService;

	final static Logger logger = Logger.getLogger(EmployeeServiceImpl.class);

	@Value("${employee.salary.detail.email.subject}")
	private String employeeSalaryDetailsSubject;

	@Override
	public SendEmployeeSalaryDetailsResponse sendEmployeeSalaryEmail(
			SendEmployeeSalaryDetailsRequest sendEmployeeSalaryDetailsRequest) {

		SendEmployeeSalaryDetailsResponse response = new SendEmployeeSalaryDetailsResponse();

		try {

			String bodyContent = mailTemplateService
					.getEmployeeSalaryDetailBodyTemplate(sendEmployeeSalaryDetailsRequest.getSalaryAmount());
			SendEmailRequest request = new SendEmailRequest();
			// Set To email address. You can set multiple email addresses using comma
			// separated string.
			request.setTo(sendEmployeeSalaryDetailsRequest.getEmail() + ",");
			// Set email subject title.
			request.setSubject(employeeSalaryDetailsSubject);
			// Set body content using mail template service.
			request.setBodyContent(bodyContent);

			SendEmailResponse rs = emailService.sendEmail(request);
			response.setStatus(rs.getStatus());

		} catch (Exception e) {

			Status status = new Status();
			status.setStatusCode("ERROR");
			status.setStatusDescription("ERROR while sending email " + e.getMessage());
		}

		return response;
	}

}
