package com.cmbk.email.services;

import com.cmbk.email.domain.SendEmployeeSalaryDetailsRequest;
import com.cmbk.email.domain.SendEmployeeSalaryDetailsResponse;

/**
 * @author chanaka.k
 *
 */
public interface EmployeeService {

	SendEmployeeSalaryDetailsResponse sendEmployeeSalaryEmail(SendEmployeeSalaryDetailsRequest request);

}
