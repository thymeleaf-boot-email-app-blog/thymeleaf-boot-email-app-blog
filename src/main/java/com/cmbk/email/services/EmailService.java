package com.cmbk.email.services;

import com.cmbk.email.domain.SendEmailRequest;
import com.cmbk.email.domain.SendEmailResponse;

/**
 * @author chanaka.k
 *
 */
public interface EmailService {

	SendEmailResponse sendEmail(SendEmailRequest request);

	
}
