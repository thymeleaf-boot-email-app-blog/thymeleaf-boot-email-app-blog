package com.cmbk.email;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymeleafBootEmailAppBlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafBootEmailAppBlogApplication.class, args);
	}

}
