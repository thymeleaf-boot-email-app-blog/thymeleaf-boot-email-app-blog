package com.cmbk.email.domain;

public class SendEmployeeSalaryDetailsResponse {

	private Status status;

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
