package com.cmbk.email.domain;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.Assert;

import com.cmbk.email.domain.common.exception.BadRequestException;

public class EmployeeValidator {

	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	public static void validateSendEmployeeSalaryDetailsRequest(SendEmployeeSalaryDetailsRequest request) {

		Assert.notNull(request.getEmail(), "Employee email address is required.");
		Boolean isValidEmail = validateEmailAddress(request.getEmail());

		if (!isValidEmail) {
			throw new BadRequestException("INVALID_EMAIL_FORMAT", "Found invalid email address");
		}

	}

	// Validate email address
	public static boolean validateEmailAddress(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}
}
