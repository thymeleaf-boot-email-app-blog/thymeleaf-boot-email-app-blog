package com.cmbk.email.domain;

/**
 * @author chanaka.k
 *
 */
public class SendEmployeeSalaryDetailsRequest {

	private String email;

	private Double salaryAmount;

	public SendEmployeeSalaryDetailsRequest() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Double getSalaryAmount() {
		return salaryAmount;
	}

	public void setSalaryAmount(Double salaryAmount) {
		this.salaryAmount = salaryAmount;
	}

}
