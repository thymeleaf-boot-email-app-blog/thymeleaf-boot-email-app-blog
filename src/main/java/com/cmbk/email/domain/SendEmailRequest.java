package com.cmbk.email.domain;

import java.io.Serializable;

/**
 * @author chanaka.k
 *
 */
public class SendEmailRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String to;

	private String bodyContent;

	private String subject;

	public SendEmailRequest() {
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getBodyContent() {
		return bodyContent;
	}

	public void setBodyContent(String bodyContent) {
		this.bodyContent = bodyContent;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

}
