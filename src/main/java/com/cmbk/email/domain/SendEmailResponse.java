package com.cmbk.email.domain;

import java.io.Serializable;

/**
 * @author chanaka.k
 *
 */
public class SendEmailResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private Status status;

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
